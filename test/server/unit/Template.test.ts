import { resolve } from 'path';
import Template from '../../../src/server/libs/Template';

describe('Template', () => {
  let template: Template;

  beforeEach(() => {
    template = new Template(resolve(__dirname, '../../../src/server/views'));
  });

  it('should compile template', () => {
    template.compile('index');
  });
});
