import React, { StatelessComponent, ReactElement } from 'react';
import './styles/styles.scss';

type ApplicationProps = {};

const Application: StatelessComponent<ApplicationProps> = (): ReactElement | null => (
  <div>Application</div>
);

export default Application;
