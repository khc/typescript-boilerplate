import { hot } from 'react-hot-loader';
import React from 'react';
import { render } from 'react-dom';
import Application from './Application';

interface HotModule extends NodeModule {
  hot: {
    accept(path?: () => void, callback?: () => void): void;
  };
}

const hotModule = module as HotModule;
const HotApplication = hot(hotModule)(Application);

render(<HotApplication />, document.getElementById('application'));

hotModule.hot.accept();
