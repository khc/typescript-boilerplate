import { Request, Response, RequestHandler, Locals } from 'polka';
import pug, { Options } from 'pug';

interface RenderFunctions {
  [key: string]: (options: Options) => string;
}

class Template {
  viewsDirectory = 'views';

  renderFunctions: RenderFunctions = {};

  constructor(viewsDirectory: string) {
    this.viewsDirectory = viewsDirectory;
  }

  compile = (template: string, options?: Options): void => {
    this.renderFunctions[template] = pug.compileFile(
      `${this.viewsDirectory}/${template}.pug`,
      options,
    );
  };

  render = (template: string, locals?: Locals): RequestHandler => (
    req: Request,
    res: Response,
  ): void => {
    const html: string = this.renderFunctions[template]({
      ...res.locals,
      ...locals,
    });
    res.end(html);
  };
}

export default Template;
