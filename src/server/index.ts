import polka, { Application } from 'polka';
import helmet from 'helmet';
import morgan from 'morgan';
import Bundler from 'parcel-bundler';
import serveStatic from 'serve-static';
import { resolve } from 'path';
import Template from './libs/Template';
import { APP_NAME, APP_TITLE, APP_PORT } from './constants';

const bundler: Bundler = new Bundler(resolve(__dirname, '../client/index-dev.tsx'), {
  outFile: 'index.js',
  outDir: resolve(__dirname, '../../public'),
  watch: true,
  hmr: true,
});

const template: Template = new Template(resolve(__dirname, './views'));
template.compile('index');

const app: Application = polka();

app
  .use(helmet())
  .use(morgan('dev'))
  .use(bundler.middleware())
  .use('/public', serveStatic(resolve(__dirname, '../../public')))
  .get(
    '*',
    template.render('index', {
      APP_TITLE,
    }),
  )
  .listen(APP_PORT, () => {
    // eslint-disable-next-line
    console.log(`${APP_NAME} is listening on port ${APP_PORT}...`);
  });
