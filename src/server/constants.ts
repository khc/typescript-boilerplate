/* eslint-disable import/prefer-default-export */
export const { APP_NAME = 'application', APP_TITLE = 'Application', APP_PORT = 3000 } = process.env;
