declare module 'polka' {
  import { Server } from 'net';
  import { IncomingMessage, ServerResponse } from 'http';
  import Trouter from 'trouter';

  export interface InterfaceError extends Error {
    message: string;
    code?: number;
    stack?: string;
    status?: number;
    details?: any;
  }

  export interface Locals {
    [key: string]: any;
  }

  export interface Response extends ServerResponse {
    locals?: Locals;
  }

  export type Request = IncomingMessage;
  export type NextHandler = (err?: Error | string) => void;
  export type UseHandler = RequestHandler | Application;

  export type RequestHandler = (req: Request, res: Response, next?: NextHandler) => void;

  export interface Application extends Trouter<RequestHandler> {
    readonly server: Server;
    use(...handlers: UseHandler[]): this;
    use(pattern: string, ...handlers: UseHandler[]): this;
    listen(port: number | string, callback?: (err?: Error) => void): this;
  }

  export default function Application(options?: any): Application;
}
