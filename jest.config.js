module.exports = {
  testMatch: ['**/test/**/*.test.{js,ts,jsx,tsx}'],
  setupFilesAfterEnv: ['./test/setup.ts'],
  coverageReporters: ['text', 'lcov'],
  collectCoverageFrom: [
    '**/src/**/*.{js,ts,jsx,tsx}',
    '!**/src/server/constants.{js,ts}',
    '!**/src/server/index.{js,ts}',
    '!**/src/server/index-dev.{js,ts}',
  ],
  preset: 'ts-jest',
};
